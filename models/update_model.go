package models

import (
	"time"
)

type UserUpdate struct {
	Name     string `json:"name,omitempty" bson:"name,omitempty" validate:"minLen:3|maxLen:50" message:"minLen:name must be at least 3 characters|maxLen:name must be less than 50 characters"`
	Email    string `json:"email,omitempty" bson:"email,omitempty" validate:"email" message:"email:email is invalid"`
	Username string `json:"username,omitempty" bson:"username,omitempty" validate:"minLen:5|maxLen:50" message:"minLen:username must be at least 5 characters|maxLen:username must be less than 50 characters"`

	LastUpdate time.Time `json:"last_update,omitempty" bson:"last_update,omitempty"`
}
