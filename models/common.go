package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type InsertOneResult struct {
	InsertedID primitive.ObjectID `json:"InsertedID"`
}

type DeleteOneResult struct {
	DeletedCount int64 `json:"DeletedCount"`
}

type UpdateOneResult struct {
	MatchedCount  int64 `json:"MatchedCount"`
	ModifiedCount int64 `json:"ModifiedCount"`
}

type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
