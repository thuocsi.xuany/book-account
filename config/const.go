package config

const (
	PageSize      int64  = 2
	DefaultAvatar string = "https://thelifetank.com/wp-content/uploads/2018/08/avatar-default-icon.png"

	DuplicatedTitleErrorMsg     = "Duplicated title"
	DuplicatedNameErrorMsg      = "Duplicated name"
	MissingNameErrorMsg         = "Missing name"
	MissingTitleErrorMsg        = "Missing title"
	NonExistentAuthorErrorMsg   = "Author list contains at least one non-existent author"
	NonExistentCategoryErrorMsg = "Category list contains at least one non-existent category"
	EmptyTitleErrorMsg          = "Title cannot be empty"
	EmptyNameErrorMsg           = "Name cannot be empty"
	NotFoundBookIDErrorMsg      = "This book does not exist"
	NotFoundAuthorIDErrorMsg    = "This author does not exist"
	NotFoundCategoryIDErrorMsg  = "This category does not exist"
	InvalidIDErrorMsg           = "the provided hex string is not a valid ObjectID"
	MaxTitleLength              = 30
	TitleTooLongErrorMsg        = "Title is too long (maximum is 30 characters)"
	InvalidYearErrorMsg         = "Invalid year"
	InvalidPriceErrorMsg        = "Invalid price"
	InvalidCoverErrorMsg        = "Invalid cover"
	InvalidPageErrorMsg         = "Invalid page"
	NotFoundPageErrorMsg        = "This page does not exist"

	// user
	InvalidEmailErrorMsg = "email is invalid"
	IncorrectPasswordErrorMsg = "Incorrect password"
	DuplicatedUsernameErrorMsg = "Username is already taken"
	DuplicatedEmailErrorMsg = "Email is already taken"
	NotFoundUsernameErrorMsg = "This user does not exist"
	InvalidTokenErrorMsg = "Invalid token"
	AnErrorOccurredErrorMsg = "An error occurred"
	UserAlreadyLoggedOutErrorMsg = "User is already logged out"
	LoggedOutMsg = "logged out"
	CouldNotLoginMsg = "could not login"
	RequiredPasswordMsg = "password is required"
	RequiredUsernameMsg = "username is required"
	ResetPasswordMsg = "Password has been reset"
	IncorrectOldPasswordMsg = "Old password is incorrect"
	ExpiredTokenMsg = "Token is expired"
	InvalidSignature = "signature is invalid"
)
