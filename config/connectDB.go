package config

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	GlobalClient         *mongo.Client
	GlobalUserCollection *mongo.Collection
)

func ConnectDB() {
	// connect to db
	clientOptions := options.Client().ApplyURI("mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/account?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.Ping(context.TODO(), nil); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")
	GlobalClient = client

	// get collection
	GlobalUserCollection = client.Database("account").Collection("user")
}
