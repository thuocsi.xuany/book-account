module account

go 1.16

require (
	github.com/dtm-labs/dtm-examples v0.0.0-20220406014226-1df24e79096c
	github.com/dtm-labs/dtmcli v1.13.0
	github.com/gin-gonic/gin v1.7.7
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gookit/validate v1.3.1
	github.com/joho/godotenv v1.4.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.1 // indirect
	github.com/stretchr/testify v1.7.1
	go.mongodb.org/mongo-driver v1.9.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
)
