package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"os"

	. "account/config"

	. "account/models"

	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func BorrowBookController(c echo.Context) error {
	username := c.Get("username").(string)

	book_id, err := primitive.ObjectIDFromHex(c.Param("book_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	// get ID of user
	var user User
	err = GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: err.Error(),
			})
		}
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	borrowBook := BorrowBook{
		UserID: user.ID,
		BookID: book_id,
	}
	json_data, err := json.Marshal(borrowBook)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	bookAPI := os.Getenv("BOOK_API_URL")
	response, err := http.Post(bookAPI+"/books/borrow", "application/json", bytes.NewBuffer(json_data))

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	if response.StatusCode != http.StatusAccepted {
		var res Response
		json.NewDecoder(response.Body).Decode(&res)
		return c.JSON(response.StatusCode, res)
	}

	var insertResult InsertOneResult
	json.NewDecoder(response.Body).Decode(&insertResult)
	return c.JSON(response.StatusCode, insertResult)
}

func ReturnBookController(c echo.Context) error {
	username := c.Get("username").(string)

	borrow_book_id, err := primitive.ObjectIDFromHex(c.Param("borrow_book_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	// get ID of user
	var user User
	err = GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: err.Error(),
			})
		}
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	returnBook := ReturnBook{
		UserID: user.ID,
	}
	json_data, err := json.Marshal(returnBook)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	bookAPI := os.Getenv("BOOK_API_URL")
	response, err := http.Post(bookAPI+"/books/return/"+borrow_book_id.Hex(), "application/json", bytes.NewBuffer(json_data))

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	if response.StatusCode != http.StatusAccepted {
		var res Response
		json.NewDecoder(response.Body).Decode(&res)
		return c.JSON(response.StatusCode, res)
	}

	var insertResult InsertOneResult
	json.NewDecoder(response.Body).Decode(&insertResult)
	return c.JSON(response.StatusCode, insertResult)

}
