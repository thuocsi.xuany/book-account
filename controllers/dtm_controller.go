package controllers

import (
	. "account/config"
	. "account/models"
	"context"
	"errors"
	"sync"

	"github.com/dtm-labs/dtmcli"
	"github.com/dtm-labs/dtmcli/dtmimp"
	"github.com/dtm-labs/dtmcli/logger"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	mongoOnce sync.Once
	mongoc    *mongo.Client
)

// MongoGet get mongo client
func MongoGet() *mongo.Client {
	mongoOnce.Do(func() {
		uri := "mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/account?retryWrites=true&w=majority"
		ctx := context.Background()
		client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
		dtmimp.E2P(err)
		mongoc = client
	})
	return mongoc
}

// MustBarrierFromGin 1
func MustBarrierFromGin(c *gin.Context) *dtmcli.BranchBarrier {
	ti, err := dtmcli.BarrierFromQuery(c.Request.URL.Query())
	logger.FatalIfError(err)
	return ti
}
func BorrowBookTCCTry(c *gin.Context) interface{} {
	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}

	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {
		// Check user is exist
		var user User
		if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"_id": borrowBook.UserID}).Decode(&user); err != nil {
			return err
		}

		// check remaining times to borrow book
		if user.Remaining <= 0 {
			return errors.New("remaining times to borrow book is 0")
		}

		return nil
	})
}

func BorrowBookTCCConfirm(c *gin.Context) interface{} {

	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}

	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {

		// decrease remaining times to borrow book
		_, err := GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"_id": borrowBook.UserID}, bson.M{"$inc": bson.M{"remaining": -1}})
		if err != nil {
			return err
		}

		return nil
	})

}

func BorrowBookTCCCancel(c *gin.Context) interface{} {

	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}

	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {

		// increase remaining times to borrow book
		_, err := GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"_id": borrowBook.UserID}, bson.M{"$inc": bson.M{"remaining": 1}})
		if err != nil {
			return err
		}

		return nil
	})
}
