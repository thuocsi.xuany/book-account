package routes

import (
	c "account/controllers"

	"github.com/dtm-labs/dtm-examples/dtmutil"
	"github.com/gin-gonic/gin"
)

func DTMSubroute() {
	app := gin.Default()
	app.POST("/dtm/borrow-book-tcc-try", dtmutil.WrapHandler2(c.BorrowBookTCCTry))
	app.POST("/dtm/borrow-book-tcc-confirm", dtmutil.WrapHandler2(c.BorrowBookTCCConfirm))
	app.POST("/dtm/borrow-book-tcc-cancel", dtmutil.WrapHandler2(c.BorrowBookTCCCancel))
	app.Run(":2000")
}
