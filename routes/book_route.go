package routes

import (
	c "account/controllers"
	m "account/middlewares"

	"github.com/labstack/echo"
)

func BookSubroute(g *echo.Group) {
	g.POST("/borrow-book/:book_id", c.BorrowBookController, m.VerifyToken)
	g.POST("/return-book/:borrow_book_id", c.ReturnBookController, m.VerifyToken)
}
